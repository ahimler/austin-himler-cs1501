//package hw4;
public class KnapsackSolver {

	public static int[][] buildTable(Order[] orders, int costLimit, int timeLimit){
	// Problem #1
	// Fill in this method to create a (costLimit + 1) by (timeLimit + 1) table
	// that for each (i, j) stores the maximum number of cookies that can be
	// produced with cost at most i in time at most j.
		int table[][] = new int[costLimit + 1][timeLimit + 1];
		
		for (int x = 0; x < costLimit+1; x++) {
			for (int y = 0; y < timeLimit+1; y++) {
				if (x == 0 || y == 0)
					table[x][y] = 0;
				for (Order o: orders) {
					if (x - o.cost < 0 || y - o.time < 0)
						table[x][y] = 0;
					else if (x - o.cost == 0 && y - o.time == 0)
						table[x][y] = o.numberOfCookies;
					else if (x - o.cost > 0 && y - o.time > 0)
						table[x][y] = Math.max(table[x - o.cost][y - o.time] + o.numberOfCookies, table[x-1][y-1]);		
					else if (x - o.cost > 0 && y - o.time < 0)
						table[x][y] = Math.max(table[x][y-o.time] + o.numberOfCookies, table[x-1][y-1]);
					else if (x - o.cost < 0 && y - o.time > 0)
						table[x][y] = Math.max(table[x-o.cost][y] + o.numberOfCookies, table[x-1][y-1]);
				}
			}
		}
		/*
		for (int a=0;a<costLimit+1;a++){
			for (int b=0;b<timeLimit+1;b++){
				if (b == timeLimit)
					System.out.println(table[a][b]);
				else
					System.out.print(table[a][b]+ " ");
			}
		}
		*/
		return table;
	}

	public static Multiset solve(Order[] orders, int costLimit, int timeLimit){
	// Problem #2
	// Fill in this method to create a multiset that represents a combination of
	// cookie choices that maximizes the number of cookies with cost at most 
	// costLimit in time at most timeLimit.  Note: You can call buildTable as
	// a subroutine.
		Multiset tempSet = new Multiset();
		int table[][] = buildTable(orders, costLimit, timeLimit);
		int currCookies = table[costLimit][timeLimit];
		for (Order o: orders) {
			while (o.numberOfCookies <= currCookies) {
				currCookies-=o.numberOfCookies;
				tempSet.add(o);
			}
		}
		return tempSet;
	}
	
}