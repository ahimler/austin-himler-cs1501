
public class ChainingHashMap{
	Node[] array;
	int size;
	
	public ChainingHashMap(int size){
		this.size = size;
		array = new Node[size];
	}

	public Integer get(Word key) {
	// Problem #1A
	// Fill in this method to get the value corresponding
	// with the key. Note: if the key is not found, then
	// return null.
		Integer index = new Integer(key.hashCode()%(size-1));
		Node currNode = array[index.intValue()];
		while (currNode != null){
			if (currNode.word.equals(key))
				return new Integer(currNode.frequency);
			currNode = currNode.next;
		}
		return null;
	}

	public void put(Word key, Integer value) {
	// Problem #1B
	// Fill in this method to insert a new key-value pair into
	// the map or update the existing key-value pair if the
	// key is already in the map.
		Integer index = new Integer(key.hashCode()%(size-1));
		Node currNode = array[index.intValue()];
		while (currNode != null){
			if (currNode.word.equals(key))
				break;
			currNode = currNode.next;
		}
		if (currNode != null)
			currNode.frequency = value;
		else {
			array[index.intValue()] = new Node(key, value, array[index.intValue()]);
		}
	}

	public Integer remove(Word key) {
	// Problem #1C
	// Fill in this method to remove a key-value pair from the
	// map and return the corresponding value. If the key is not
	// found, then return null.
		Integer index = new Integer(key.hashCode()%(size-1));
		if (array[index.intValue()] == null)
			return null;
		if (array[index.intValue()].word.equals(key)){
			Integer i = new Integer(array[index.intValue()].frequency);
			array[index.intValue()] = array[index.intValue()].next;
			return i;
		}
		Node lastNode = array[index.intValue()];
		Node currNode = lastNode.next;
		while (currNode != null && !currNode.word.equals(key)){
			currNode = currNode.next;
			lastNode = currNode;
		}
		if (currNode != null){
			Integer j = new Integer(currNode.frequency);
			lastNode.next = currNode.next;
			return j;
		}
		return null;
	}
	
	// This method returns the total size of the underlying array.
	// In other words, it returns the total number of linkedlists.
	public int getSize(){
		return size;
	}
	
	// This method counts how many keys are stored at the given array index.
	// In other words, it computes the size of the linkedlist at the given index.
	public int countCollisions(int index){
		if(index < 0 || index >= size) return -1;
		
		int count = 0;
		Node temp = array[index];
		while(temp != null){
			temp = temp.next;
			count++;
		}
		
		return count;
	}
	
}
