package hw1;
public class Selector {
	
	private static void swap(Word[] array, int i, int j){
		if(i == j) return;
		
		Word temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	
	public static Word[] select(Word[] array, int k){
	// Problem #1
	// Fill in this method with an O(n*k) time algorithm
	// that returns the k largest elements of array in
	// order from largest to smallest.
	// Note: This should return an array with k elements.
		if (array == null)
			return null;
		else if (k > array.length)
			return null;
		else {
			Word[] tempArray = new Word[k];
			for (int a = 0; a < k; a++) {
				for (int b = a + 1; b < array.length; b ++) {
					if (array[b].compareTo(array[a]) == 1)
					swap(array, a, b);
				}	
				tempArray[a] = array[a];
			}
			return tempArray;
		}
	}
}
