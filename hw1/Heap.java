package hw1;
public class Heap {
	private Word[] myHeap;
	private int size;
	
	public Heap(Word[] array){
		myHeap = array;
		size = array.length;
		buildHeap();
	}
	
	private int getLeftChild(int a) {
		return a * 2 + 1;
	}

	private void swap(int a, int b){
		Word temp = myHeap[a];
		myHeap[a] = myHeap[b];
		myHeap[b] = temp;
	}
	private void sink(int a){
		while (!isLeaf(a)) {
			int b = getLeftChild(a);
			if (b < size-1 && myHeap[b].compareTo(myHeap[b+1]) == -1)
				b++;
			if (myHeap[a].compareTo(myHeap[b]) == 1 || myHeap[a].compareTo(myHeap[b]) == 0)
				return;
			else {
				swap(a, b);
				a = b;
			}
		}
	}

	public void buildHeap(){
	// Problem #2
	// Fill in this method with an O(n) time algorithm
	// that builds an n element complete binary heap.
	// Note: You are allowed to add and modify fields
    // and helper methods.
		for (int a = size/2-1; a >= 0; a--) {
			sink(a);
		}
	}
	
	public Word removeMax(){
	// Problem #3
	// Fill in this method with an O(log(n)) time algorithm
	// that removes the root element, restores the heap
	// structure, and finally returns the removed root element.
		if (size == 0)
			return null;
		else {
			Word temp = myHeap[0];
			swap(0, size-1);
			size--;
			sink(0);
			return temp;
		}
	}
	
	public boolean isLeaf(int a){
		return (a >= size/2 - 1) && (a < size - 1);
	}
	
	public Word[] select(int k){
		Word[] result = new Word[k];
		for(int i = 0; i < k; i++){
			result[i] = this.removeMax();
		}
		return result;
	}
}
