package hw3;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class BFS {
	private static Queue<Location> q = new LinkedList<Location>();
	private static Set<Location> visited = new HashSet<Location>();
	private static Set<Location> reachableSet = new HashSet<Location>();
	
	// Returns the set of reachable locations using breadth first search
	public static Set<Location> getReachableSet(WeightedGraph graph, Location start){
	// Problem #1
	// Fill in this method to compute the set of all possible reachable 
	// locations (ignoring costs and budget).  You must use Breadth
	// First Search to get full credit.	
		visited.add(start);
		q.add(start);
		while(!q.isEmpty()){
			Location temp = q.remove();
			if (!reachableSet.contains(temp))
				reachableSet.add(temp);
			Iterator<Location> i = graph.getNeighbors(temp).iterator();
			while (i.hasNext()){
					addVisited_addQueue(i.next());
			}
		}
		return reachableSet;
	}
	
	public static void addVisited_addQueue(Location l){
		if (visited.contains(l))
			return;
		else {
			visited.add(l);
			q.add(l);
		}
	}

}
