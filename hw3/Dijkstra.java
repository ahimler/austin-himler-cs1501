package hw3;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")
public class Dijkstra {
	
	private static Set<Location> visited = new HashSet<Location>();
	private static Map<Location, Integer> distance = new HashMap<Location, Integer>();
	private static Set<Location> destinationSet = new HashSet<Location>();
	
	// Returns the set of destinations that can be reached without going over budget
	public static Set<Location> getDestinationSet(WeightedGraph graph, Location start, Integer budget){
	// Problem #2
	// Fill in this method to compute the set of all possible destinations 
	// that can be reached while staying within the travel budget. You must
	// use Dijkstra's algorithm to get full credit.  We encourage you to
	// implement the |V|^2 time version of Dijkstra's algorithm.  You are
	// free to add helper methods. 			
		Location current = start;
		//set current node distance to zero
		distance.put(start, 0);
		destinationSet.add(start);
		//set neighbors to infinity
		for (Location a: graph.vertices){
			if (a.equals(current)) distance.put(current, 0);
			else distance.put(a, Integer.MAX_VALUE);
		}
		//while money is left, explore map
		while (budget.intValue() >= 0){
			//update neighbors distances as previously stored distance or
			//							 as current location distance plus cut-edge distance
			for (Location b: graph.getNeighbors(current)){
				if (!visited.contains(b) && !distance.get(b).equals(Integer.MAX_VALUE)) {
					distance.put(b, graph.getWeight(current, b));
				} else if (!visited.contains(b) && (graph.getWeight(current, b) + distance.get(current)) < distance.get(b)) {
					distance.put(b, distance.get(current) + graph.getWeight(current, b));
				} else 
					distance.put(b, distance.get(b));
			}
			//add current to visited
			visited.add(current);
			//find shortest path from current
			Integer temp = Integer.MAX_VALUE;
			for (Location c: graph.getNeighbors(current)){
				if (distance.get(c) < temp && !visited.contains(c)) {
					temp = distance.get(c);
					current = c;
				}
			}
			budget -= distance.get(current);
			if (budget >= 0)
				destinationSet.add(current);
		}
			return destinationSet;
	}
}
